export class CountService {
  counter: number = 0;

  count() {
    this.counter++;
    console.log('usersService was called x times', this.counter);
  }
}

