import { Component } from '@angular/core';
import bla from './myConst';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent {
  constructor() {
    const { ca } = bla.c;
    const { ea } = bla.e;
    console.log(ca);
    console.log(ea);
  }
}

