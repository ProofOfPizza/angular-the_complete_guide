import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent implements OnInit {
  submitted = false;
  defaultQuestion = 'pet';
  answer = '';
  genders = ['male', 'female', 'other'];
  selectedGender: string;
  user = {
    username: '',
    email: '',
    secret: '',
    answer: '',
    gender: '',
    genderfield: '',
  };

  @ViewChild('f') signupForm: NgForm;

  ngOnInit() {
    this.selectedGender = this.genders[1];
  }

  suggestUserName() {
    const suggestedName = 'Superuser';
    // this.signupForm.setValue({
    //   userdata: { username: suggestedName, email: '' },
    //   secret: 'pet',
    //   questionAnswer: 'nahh',
    //   gender: 'male',
    // });
    this.signupForm.form.patchValue({ userdata: { username: suggestedName } });
  }
  onSubmit() {
    this.user.username = this.signupForm.value.userdata.username;
    this.user.email = this.signupForm.value.userdata.email;
    this.user.secret = this.signupForm.value.secret;
    this.user.answer = this.signupForm.value.questionAnswer;
    this.user.gender = this.signupForm.value.gender;
    this.user.genderfield = this.signupForm.value.genderOtherValue;
    this.submitted = true;

    this.signupForm.reset();
  }
}

