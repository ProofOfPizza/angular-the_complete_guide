import { Injectable } from '@angular/core';

@Injectable() // not necessary but recommended for newer versions of angular. In the future will be required.
export class LoggingService {
  logStatusChange(newStatus: string) {
    console.log(`A server status changed, new status: ${newStatus}`);
  }
}

