import { Injectable } from '@angular/core';
import { LoggingService } from './logging.service';

@Injectable()
export class AccountsService {
  accounts = [
    {
      name: 'Master Account',
      status: 'active',
    },
    {
      name: 'Testaccount',
      status: 'inactive',
    },
    {
      name: 'Hidden Account',
      status: 'unknown',
    },
  ];
  constructor(private logginService: LoggingService) {}

  updateAccount(updateInfo: { id: number; newStatus: string }) {
    this.accounts[updateInfo.id].status = updateInfo.newStatus;
    this.logginService.logStatusChange(updateInfo.newStatus);
  }

  addAccount(newAccount: { name: string; status: string }) {
    this.accounts.push(newAccount);
    this.logginService.logStatusChange(newAccount.status);
  }
}

