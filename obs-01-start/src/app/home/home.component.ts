import { Component, OnDestroy, OnInit } from '@angular/core';
import { interval, Subscription, Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css'],
})
export class HomeComponent implements OnInit, OnDestroy {
  private firstObs: Subscription;
  private secondObs: Subscription;
  private customObs: Observable<number | Error>;
  constructor() {}

  ngOnInit() {
    // this.firstObs = interval(1000).subscribe((count) => {
    //   console.log(count);
    // });
    //
    this.customObs = new Observable((observer) => {
      let count = 0;
      const n = Math.floor(Math.random() * 4);
      setInterval(() => {
        observer.next(count);
        if (count === 3) {
          observer.complete();
        }
        if (count % n === 0) {
          observer.error(new Error(`count mod ${n} was zero!`));
        }
        count++;
      }, 1000);
    });

    this.secondObs = this.customObs
      .pipe(
        map((x: number) => {
          const y: number = Math.floor((x + 3) / 3);
          return `meppie ${y}`;
        }),
      )
      .subscribe((mappedData) => {
        console.log(mappedData);
      });

    this.firstObs = this.customObs.subscribe(
      (counter) => {
        console.log(counter);
      },
      (error) => {
        alert(error.message);
      },
      () => {
        console.log('mission complete!');
      },
    );
  }
  ngOnDestroy() {
    this.firstObs.unsubscribe();
  }
}

