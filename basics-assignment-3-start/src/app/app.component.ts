import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent {
  displayEnabled: boolean = false;
  clicks: Array<string> = [];
  onDisplay() {
    this.displayEnabled = !this.displayEnabled;
    this.clicks.push(Date.now().toString());
  }
}

