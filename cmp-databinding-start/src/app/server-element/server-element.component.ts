import {
  AfterContentChecked,
  AfterContentInit,
  AfterViewChecked,
  AfterViewInit,
  Component,
  ContentChild,
  DoCheck,
  ElementRef,
  Input,
  OnChanges,
  OnDestroy,
  OnInit,
  SimpleChanges,
  ViewChild,
} from '@angular/core';

@Component({
  selector: 'app-server-element',
  templateUrl: './server-element.component.html',
  styleUrls: ['./server-element.component.css'],
})
export class ServerElementComponent
  implements
    OnInit,
    OnChanges,
    DoCheck,
    AfterContentInit,
    AfterContentChecked,
    AfterViewInit,
    AfterViewChecked,
    OnDestroy {
  @Input('srvElement') element: {
    //arg for Input is an alias to be used in parent cpmonent html
    type: string;
    name: string;
    content: string;
  };
  @ViewChild('heading', { static: true }) header: ElementRef;
  @ContentChild('contentParagraph', { static: true }) contentParagraph: ElementRef;

  constructor() {
    console.log('consrtuctor called');
  }

  ngOnInit(): void {
    console.log('onInit called');
    console.log('header in onInit', this.header.nativeElement.textContent);
    console.log('contentParagraph in OnInit', this.contentParagraph.nativeElement.textContent);
  }

  ngOnChanges(changes: SimpleChanges) {
    console.log('onChanges called', changes);
  }

  ngDoCheck() {
    console.log('doCheck called');
  }
  ngAfterContentInit() {
    console.log('ngAfterContentInit called');
    console.log('contentParagraph in AfterContentInit', this.contentParagraph.nativeElement.textContent);
  }
  ngAfterContentChecked() {
    console.log('AfterContentChecked');
    console.log('header in afterContentChecked', this.header.nativeElement.textContent);
  }
  ngAfterViewInit() {
    console.log('AfterViewInit called');
    console.log('header in AfterViewInit', this.header.nativeElement.textContent);
  }
  ngAfterViewChecked() {
    console.log('AfterViewChecked called');
  }
  ngOnDestroy() {
    console.log('OnDestroy called');
  }
}

