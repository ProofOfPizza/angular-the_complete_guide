import { Component, OnInit } from '@angular/core';
import { AbstractControl, FormArray, FormControl, FormGroup, Validators } from '@angular/forms';
import { Observable, Observer } from 'rxjs';

enum errorz {
  UGLY = 'ugly',
  INPOSSIBLE = 'impossible',
}

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent implements OnInit {
  genders = ['male', 'female'];
  signupForm: FormGroup;
  forbiddenUsernames = ['super', 'self'];

  ngOnInit(): void {
    this.signupForm = new FormGroup({
      userdata: new FormGroup({
        username: new FormControl(null, [Validators.required, this.invalidNames.bind(this)]),
        email: new FormControl(null, [Validators.required, Validators.email], this.invalidEmail.bind(this)), //bind is prpbably unnecessary cuz we dont use this inside function
      }),
      gender: new FormControl('female'),
      hobbies: new FormArray([]),
    });

    this.signupForm.patchValue({ userdata: { email: 'blip@blap.blp' } });

    this.signupForm.valueChanges.subscribe((changes) => {
      console.log('valueChanges', changes);
    });
    this.signupForm.statusChanges.subscribe((changes) => {
      console.log('statusChanges', changes);
    });
  }
  onSubmit() {
    console.log(this.signupForm);
    this.signupForm.reset();
  }
  onAddHobby() {
    const hobbyControl = new FormControl(null, Validators.required);
    (<FormArray>this.signupForm.get('hobbies')).push(hobbyControl);
  }
  get hobbyControls(): Array<AbstractControl> {
    return (this.signupForm.get('hobbies') as FormArray).controls;
  }

  private invalidNames(control: FormControl): { [key: string]: errorz } {
    if (this.forbiddenUsernames.indexOf(control.value) > -1) {
      return {
        nameIsInvalid: errorz.UGLY,
      };
    }
    return null;
  }

  private invalidEmail(control: FormControl): Observable<any> | Promise<any> {
    return new Promise((resolve, _reject) => {
      setTimeout(() => {
        if (control.value === 'bla@bla.com') {
          resolve({ emailIsInvalid: errorz.INPOSSIBLE });
        }
        resolve(null);
      }, 1500);
    });
  }
}

