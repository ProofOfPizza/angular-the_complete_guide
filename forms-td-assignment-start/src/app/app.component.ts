import { Component, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent {
  @ViewChild('f') subForm: NgForm;
  subs = ['Advanced', 'Mwah', 'Nahh'];
  firstSub = this.subs[0];

  onSubmit() {
    console.log(this.subForm.value);
  }
  onSelectDropdownValue(value: string) {
    console.log('setting sub', value);
    console.log(this.subForm.value.subscription);
    if (this.subForm.value.subscription !== value) this.subForm.form.patchValue({ subscription: value });
  }
}

