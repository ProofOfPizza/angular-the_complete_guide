import { Component, OnInit } from '@angular/core';
import { FormArray, FormControl, FormGroup, Validators } from '@angular/forms';
import { Observable } from 'rxjs/internal/Observable';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent implements OnInit {
  projectForm: FormGroup;
  statusses: FormArray;

  ngOnInit(): void {
    this.statusses = new FormArray(['Active', 'Delayed', 'Finished'].map((status) => new FormControl(status)));
    this.projectForm = new FormGroup({
      projectName: new FormControl(null, [Validators.required, this.nameIsInvalid]),
      email: new FormControl(null, [Validators.required, Validators.email], this.emailIsInvalid),
      status: new FormControl('Active'),
    });
  }

  nameIsInvalid(control: FormControl): { [key: string]: boolean } {
    if (control.value === 'test') {
      return { nameIsInvalid: true };
    }
    return null;
  }

  emailIsInvalid(control: FormControl): Observable<{ [key: string]: boolean }> | Promise<{ [key: string]: boolean }> {
    return new Promise((resolve) => {
      setTimeout(() => {
        if (control.value.indexOf('@test.tst') > -1) {
          resolve({ emailIsInvalid: true });
        }
        resolve(null);
      }, 1500);
    });
  }

  get statusControls() {
    return (<FormArray>this.statusses).controls;
  }

  onSubmit() {
    console.log(this.projectForm);
  }
}

