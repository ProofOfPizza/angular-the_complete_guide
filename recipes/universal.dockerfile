FROM node:lts-alpine3.12 as build
WORKDIR /app
COPY . .
RUN npm install && npm run build:ssr
EXPOSE 4200
CMD [ "npm", "run", "serve:ssr" ]
