import { HttpClient, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map, tap } from 'rxjs/operators';
import { Recipe } from 'src/app/components/recipe-book/recipes/recipe.model';
import { RecipeService } from 'src/app/components/recipe-book/recipes/recipe.service';
import { match, __ } from 'ts-pattern';

@Injectable({
  providedIn: 'root',
})
export class DataStorageService {
  private dbUrl = 'https://recipes-69172-default-rtdb.europe-west1.firebasedatabase.app/recipes.json';

  constructor(private http: HttpClient, private recipeService: RecipeService) {}

  saveRecipes() {
    this.http.put(this.dbUrl, this.recipeService.recipes).subscribe((response: HttpResponse<any>) => {
      console.log(response);
    });
  }

  fetchRecipes() {
    return this.http.get<Array<Recipe>>(this.dbUrl).pipe(
      map((recipes) => {
        return recipes.map((recipe: Recipe) => {
          match(recipe)
            .with({ ingredients: undefined }, () => {
              recipe.ingredients = [];
            })
            .otherwise(() => {
              recipe;
            });
          return recipe;
        });
      }),
      tap((recipes) => {
        this.recipeService.resetRecipesFromServer(recipes);
      }),
    );
  }
}

