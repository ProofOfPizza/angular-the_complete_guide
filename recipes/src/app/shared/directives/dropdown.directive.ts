import { Directive, ElementRef, HostBinding, HostListener, OnInit, Renderer2 } from '@angular/core';

@Directive({
  selector: '[appDropdown]',
})
export class DropdownDirective implements OnInit {
  @HostBinding('class.open') isOpen: boolean;

  constructor(private elementRef: ElementRef) {}

  ngOnInit() {
    this.isOpen = false;
  }

  @HostListener('document:click', ['$event']) toggleDropdown(event: Event) {
    this.isOpen = this.elementRef.nativeElement.contains(event.target) ? !this.isOpen : false;
  }
}

