import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { RouterModule, Routes } from '@angular/router';
import { CoreModule } from 'src/app/core/core.module';
import { SharedModule } from 'src/app/shared/shared.module';
import { AuthComponent } from './auth.component';

const routes: Routes = [{ path: '', component: AuthComponent }];
@NgModule({
  declarations: [AuthComponent],
  imports: [CoreModule, SharedModule, ReactiveFormsModule, RouterModule.forChild(routes)],
})
export class AuthModule {}

