import { HttpClientTestingModule } from '@angular/common/http/testing';
import { CUSTOM_ELEMENTS_SCHEMA, PLATFORM_ID } from '@angular/core';
import { RouterTestingModule } from '@angular/router/testing';
import { createServiceFactory, SpectatorService } from '@ngneat/spectator';
import { BehaviorSubject } from 'rxjs';
import { AppRoutingModule } from 'src/app/app.routing.module';
import { AuthGuard } from './auth.guard';
import { AuthService } from './auth.service';
import { User } from './user.modal';

describe('AuthGuard', () => {
  let spectator: SpectatorService<AuthGuard>;
  let platformUnderTest: string = 'server';
  const config = {
    service: AuthGuard,
    imports: [RouterTestingModule, HttpClientTestingModule],
    providers: [
      AppRoutingModule,
      AuthService,
      { provide: PLATFORM_ID, useFactory: () => platformUnderTest },
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA],
  };
  const createService = createServiceFactory(config);
  beforeEach(() => {
    platformUnderTest = 'server';
  });

  it('should should return true if running on server', () => {
    spectator = createService();
    const result = spectator.service.canActivate(null, null);
    expect(result).toEqual(true);
  });

  it('should should return true if running on browser and authenticated', async (done: DoneFn) => {
    platformUnderTest = 'browser';
    spectator = createService();
    spyOn(spectator.service['authService'], 'autoLogin').and.callFake(() => {});
    const user = new User(
      'bla@bla.com',
      'id',
      'token',
      new Date(new Date().getTime() + 10000)
    );
    spectator.service['authService'].user.next(user);
    spectator.service['authService'].autoLogin();
    const result = await spectator.service.canActivate(null, null);
    if (typeof result === 'boolean') {
      throw new Error(
        'Shoudl not be true, because platfor is browser and false is impossible'
      );
    } else {
      result.subscribe((r) => {
        expect(r).toEqual(true);
        done();
      });
    }
  });

  it('should should navigate to /auth if running on browser and not authenticated', async (done: DoneFn) => {
    platformUnderTest = 'browser';
    spectator = createService();
    spyOn(spectator.service['authService'], 'autoLogin').and.callFake(() => {});
    const spyNavigate = spyOn(
      spectator.service['router'],
      'navigate'
    ).and.callThrough();
    spectator.service['authService'].user.next(null);
    const result = await spectator.service.canActivate(null, null);
    if (typeof result === 'boolean') {
      throw new Error('result should not be true, and can never be false');
    } else {
      result.subscribe((r) => {
        expect(spyNavigate).toHaveBeenCalled();
        expect(r).toEqual(false);
        done();
      });
    }
  });
});

