import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { AuthResponse, AuthService } from './auth.service';

@Component({
  selector: 'app-auth',
  templateUrl: './auth.component.html',
  styleUrls: ['./auth.component.css'],
})
export class AuthComponent implements OnInit {
  authForm: FormGroup;
  loginMode = true;
  isLoading = false;
  error: string = null;
  constructor(private authService: AuthService, private router: Router) {}

  ngOnInit(): void {
    this.initializeForm();
  }
  initializeForm() {
    this.authForm = new FormGroup({
      email: new FormControl(null, [Validators.email, Validators.required]),
      password: new FormControl(null, [Validators.required, Validators.minLength(6)]),
      repeatPassword: new FormControl(
        null,
        this.loginMode ? [] : [Validators.required, this.passwordsMatch.bind(this)],
      ),
    });
  }
  passwordsMatch(repeatPasswordControl: FormControl): { [key: string]: boolean } {
    if (!this.loginMode && this.authForm && repeatPasswordControl.value !== this.authForm.get('password').value) {
      return { passwordsMismatch: true };
    }
    return null;
  }

  onSwitchMode() {
    this.loginMode = !this.loginMode;
    this.resetError();
    this.initializeForm();
  }

  resetError() {
    this.error = null;
  }

  onHandleError() {
    this.resetError();
  }

  onSubmit() {
    const authObservable: Observable<AuthResponse> = this.loginMode
      ? this.authService.login(this.authForm.value.email, this.authForm.value.password)
      : this.authService.signUp(this.authForm.value.email, this.authForm.value.password);
    this.error = null;

    this.isLoading = true;
    authObservable.subscribe(
      (_response) => {
        this.isLoading = false;
        this.router.navigate(['/recipes']);
      },
      (error) => {
        console.log(error);
        this.error = error;
        this.isLoading = false;
      },
    );
    this.authForm.reset();
  }
}

