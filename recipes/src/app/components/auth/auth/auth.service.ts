import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { BehaviorSubject, Observable, throwError } from 'rxjs';
import { catchError, tap } from 'rxjs/operators';
import { match } from 'ts-pattern';
import { User } from './user.modal';
import { environment } from '../../../../environments/environment';

export interface AuthResponse {
  idToken: string;
  email: string;
  refreshToken: string;
  expiresIn: string;
  localId: string;
  registered?: boolean;
}

@Injectable({
  providedIn: 'root',
})
export class AuthService {
  readonly singUpUrl = environment.singUpUrl;
  readonly loginUrl = environment.loginUrl;
  user = new BehaviorSubject<User>(null);
  timer: ReturnType<typeof setTimeout>;

  constructor(private http: HttpClient, private router: Router) {}

  signUp(email: string, password: string): Observable<AuthResponse> {
    const body = { email, password, returnSecureToken: true };
    return this.http
      .post<AuthResponse>(this.singUpUrl, body)
      .pipe(catchError(this.errorHandler), tap(this.authenticationHandler.bind(this)));
  }
  login(email: string, password: string): Observable<AuthResponse> {
    const body = { email, password, returnSecureToken: true };
    return this.postLogin(body).pipe(catchError(this.errorHandler), tap(this.authenticationHandler.bind(this)));
  }

  logout() {
    this.user.next(null);
    localStorage.removeItem('user');
    this.router.navigate(['/auth']);
    if (this.timer) {
      clearTimeout(this.timer);
    }
    this.timer = null;
  }

  autoLogin(): void {
    const userData: {
      email: string;
      id: string;
      _token: string;
      _expirationDate: string;
    } = JSON.parse(localStorage.getItem('user'));
    if (!userData) {
      return;
    }
    const user: User = new User(userData.email, userData.id, userData._token, new Date(userData._expirationDate));
    if (user.token) {
      this.user.next(user);
      const expirationDuration: number = new Date(userData._expirationDate).getTime() - new Date().getTime();
      this.autoLogout(expirationDuration);
    }
  }

  autoLogout(expirationDuration: number) {
    this.timer = setTimeout(() => {
      this.logout();
    }, expirationDuration);
  }

  private postLogin(body: { email: string; password: string; returnSecureToken: boolean }): Observable<AuthResponse> {
    return this.http.post<AuthResponse>(this.loginUrl, body);
  }

  private authenticationHandler(response: AuthResponse) {
    const now = new Date().getTime();
    const expirationDate = new Date(now + +response.expiresIn * 1000);
    const user = new User(response.email, response.localId, response.idToken, expirationDate);
    this.user.next(user);
    this.autoLogout(+response.expiresIn * 1000);
    localStorage.setItem('user', JSON.stringify(user));
  }

  private errorHandler(err: any) {
    let errorMessage = '';
    match(err)
      .with({ error: { error: { message: 'OPERATION_NOT_ALLOWED' } } }, () => {
        errorMessage = 'This operation is not allowed.';
      })
      .with({ error: { error: { message: 'INVALID_PASSWORD' } } }, () => {
        errorMessage = 'This password is invalid.';
      })
      .with({ error: { error: { message: 'EMAIL_NOT_FOUND' } } }, () => {
        errorMessage = 'This email was not found. Want to sign up?';
      })
      .with({ error: { error: { message: 'EMAIL_EXISTS' } } }, () => {
        errorMessage = 'This Email exists already. Did you mean to login?';
      })
      .with({ error: { error: { message: 'OPERATION_NOT_ALLOWED' } } }, () => {
        errorMessage = 'This operation is not allowed.';
      })
      .with({ error: { error: { message: 'TOO_MANY_ATTEMPTS_TRY_LATER' } } }, () => {
        errorMessage = 'You have tried too many times. Come back later.';
      })
      .otherwise(() => {
        errorMessage = 'An unknown error occurred';
      });

    return throwError(errorMessage);
  }
}

