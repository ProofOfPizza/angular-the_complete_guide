import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { createHttpFactory, HttpMethod, SpectatorHttp } from '@ngneat/spectator';
import { AuthService } from './auth.service';
import { RouterTestingModule } from '@angular/router/testing';

describe('AuthService', () => {
  let spectator: SpectatorHttp<AuthService>;
  const createHttp = createHttpFactory({
    service: AuthService,
    imports: [RouterTestingModule],
    schemas: [CUSTOM_ELEMENTS_SCHEMA],
  });
  const store = {};
  const mockStorage = {
    getItem: (key: string) => {
      return key in store ? JSON.parse(store[key]) : null;
    },
    setItem: (key: string, value: Object) => {
      store[key] = JSON.stringify(value);
    },
  };

  beforeEach(() => {
    spectator = createHttp();
  });

  it('should exist', () => {
    expect(spectator.service).toBeTruthy();
  });

  it('should call BE on #singUp', () => {
    spectator.service.signUp('bla@bla.com', '123456').subscribe();
    const req = spectator.expectOne(spectator.service.singUpUrl, HttpMethod.POST);
    expect(req.request.body.email).toEqual('bla@bla.com');
    expect(req.request.body.password).toEqual('123456');
  });

  it('should emit new user at #authenticationHandler', () => {
    const spy = spyOn(spectator.service.user, 'next').and.callThrough();
    spectator.service['authenticationHandler']({
      idToken: 'idToken',
      email: 'bla@bla.com',
      refreshToken: 'token',
      expiresIn: '3600',
      localId: 'localId',
    });
    expect(spy).toHaveBeenCalled();
    return spectator.service.user.subscribe((user) => {
      expect(user.email).toBe('bla@bla.com');
      expect(user.token).toBe('idToken');
      expect(user.id).toBe('localId');
    });
  });

  it('should call #autoLogout at #authenticationHandler', () => {
    const spy = spyOn(spectator.service, 'autoLogout').and.callThrough();
    spectator.service['authenticationHandler']({
      idToken: 'idToken',
      email: 'bla@bla.com',
      refreshToken: 'token',
      expiresIn: '3600',
      localId: 'localId',
    });
    expect(spy).toHaveBeenCalledWith(3600 * 1000);
  });

  it('should set userdata in localStorage at #authenticationHandler', () => {
    const spySet = spyOn(localStorage, 'setItem').and.callFake(mockStorage.setItem);
    spectator.service['authenticationHandler']({
      idToken: 'idToken',
      email: 'bla@bla.com',
      refreshToken: 'token',
      expiresIn: '3600',
      localId: 'localId',
    });
    expect(spySet).toHaveBeenCalled();
  });

  it('should get userdata from localStorage at #autoLogin', () => {
    const spyGet = spyOn(localStorage, 'getItem').and.callFake(mockStorage.getItem);
    spectator.service.autoLogin();
    expect(spyGet).toHaveBeenCalledWith('user');
    console.log(JSON.stringify(store));
  });
  it('should call BE on #login', () => {
    spectator.service.login('bla@bla.com', '123456').subscribe();
    const req = spectator.expectOne(spectator.service.loginUrl, HttpMethod.POST);
    expect(req.request.body.email).toEqual('bla@bla.com');
    expect(req.request.body.password).toEqual('123456');
  });
});

