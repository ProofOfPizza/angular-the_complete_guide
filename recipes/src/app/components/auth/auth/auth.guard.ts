import { isPlatformServer } from '@angular/common';
import { Inject, Injectable, PLATFORM_ID } from '@angular/core';
import {
  ActivatedRouteSnapshot,
  CanActivate,
  Router,
  RouterStateSnapshot,
} from '@angular/router';
import { Observable } from 'rxjs';
import { map, take } from 'rxjs/operators';
import { AuthService } from './auth.service';

@Injectable({
  providedIn: 'root',
})
export class AuthGuard implements CanActivate {
  constructor(
    private authService: AuthService,
    private router: Router,
    @Inject(PLATFORM_ID) private platformId: Object
  ) {}

  canActivate(
    _route: ActivatedRouteSnapshot,
    _state: RouterStateSnapshot
  ): boolean | Promise<boolean> | Observable<boolean> {
    if (isPlatformServer(this.platformId)) {
      console.log('server');
      return true;
    } else {
      console.log('browser');
      this.authService.autoLogin();
      return this.authService.user.pipe(
        take(1),
        map((user) => {
          console.log('user', user);
          if (!!user) {
            return true;
          }
          console.log('navigate');
          this.router.navigate(['/auth']);
          return false;
        })
      );
    }
  }
}

