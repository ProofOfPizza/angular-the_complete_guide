import { HttpClientModule } from '@angular/common/http';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { FormControl, ReactiveFormsModule } from '@angular/forms';
import { RouterTestingModule } from '@angular/router/testing';
import { createComponentFactory, Spectator } from '@ngneat/spectator';
import { Observable, of } from 'rxjs';
import { AppRoutingModule } from 'src/app/app.routing.module';
import { AuthComponent } from './auth.component';
import { AuthResponse, AuthService } from './auth.service';

describe('AuthComponent', () => {
  let spectator: Spectator<AuthComponent>;
  const createComponent = createComponentFactory({
    component: AuthComponent,
    imports: [RouterTestingModule, HttpClientModule, ReactiveFormsModule, HttpClientTestingModule],
    providers: [AppRoutingModule, AuthService],
    schemas: [CUSTOM_ELEMENTS_SCHEMA],
    detectChanges: false,
  });

  beforeEach(() => {
    spectator = createComponent();
  });

  it('should exist', () => {
    spectator.detectChanges();
    expect(spectator.component).toBeTruthy();
  });

  it('should be in login mode by default', () => {
    spectator.detectChanges();
    expect(spectator.query('h3')).toContainText('Please Login');
    expect(spectator.query('h3')).not.toContainText('Please Sign up');
  });

  it('should switch to signup mode #onSwitchMode', () => {
    spectator.detectChanges();
    spectator.component.onSwitchMode();
    spectator.detectChanges();
    expect(spectator.query('h3')).not.toContainText('Please Login');
    expect(spectator.query('h3')).toContainText('Please Sign up');
  });

  it('should return an error with unmatching values for #passwordsMatch', () => {
    spectator.detectChanges();
    spectator.component.loginMode = false;
    spectator.component.authForm.get('password').setValue('password');
    spectator.component.authForm.get('repeatPassword').setValue('repeatPassword');
    spectator.detectChanges();
    const result = spectator.component.passwordsMatch(
      spectator.component.authForm.get('repeatPassword') as FormControl,
    );
    expect(result).toEqual({ passwordsMismatch: true });
  });
  it('should return null at #passwordsMatch if in loginMode', () => {
    spectator.detectChanges();
    spectator.component.loginMode = true;
    spectator.component.authForm.get('password').setValue('password');
    spectator.component.authForm.get('repeatPassword').setValue('');
    spectator.detectChanges();
    const result = spectator.component.passwordsMatch(
      spectator.component.authForm.get('repeatPassword') as FormControl,
    );
    expect(result).toEqual(null);
  });

  it('should reset the form  #onSubmit', () => {
    const responseData: AuthResponse = {
      idToken: 'idToken',
      email: 'email',
      refreshToken: 'refreshToken',
      expiresIn: '3600',
      localId: 'localId',
      registered: true,
    };

    spyOn<any>(spectator.component['authService'], 'postLogin').and.returnValue(
      of(responseData) as Observable<AuthResponse>,
    );

    spectator.detectChanges();
    const spyReset = spyOn(spectator.component.authForm, 'reset');
    spectator.component.loginMode = false;
    spectator.component.authForm.patchValue({
      email: new FormControl('email'),
      password: new FormControl('password'),
      repeatPassword: new FormControl(' repeatPassword'),
    });
    spectator.detectChanges();
    spectator.component.onSubmit();
    expect(spyReset).toHaveBeenCalled();
  });

  it('should navigate to recipes on a succesful login #onSubmit', () => {
    const responseData: AuthResponse = {
      idToken: 'idToken',
      email: 'email',
      refreshToken: 'refreshToken',
      expiresIn: '3600',
      localId: 'localId',
      registered: true,
    };

    spyOn<any>(spectator.component['authService'], 'postLogin').and.returnValue(
      of(responseData) as Observable<AuthResponse>,
    );

    spectator.detectChanges();
    const spyNavigate = spyOn(spectator.component['router'], 'navigate');
    spectator.component.loginMode = true;
    spectator.component.authForm.patchValue({
      email: new FormControl('email'),
      password: new FormControl('password'),
      repeatPassword: new FormControl(' repeatPassword'),
    });
    spectator.detectChanges();
    spectator.component.onSubmit();
    expect(spyNavigate).toHaveBeenCalledWith(['/recipes']);
  });

  it('should not navigate on a failed login #onSubmit', () => {
    const http = spectator.inject<HttpTestingController>(HttpTestingController);
    const spyNavigate = spyOn(spectator.component['router'], 'navigate');
    spectator.component.loginMode = true;
    spectator.detectChanges();
    spectator.component.onSubmit();
    http.expectOne(spectator.component['authService'].loginUrl).flush({}, { status: 400, statusText: 'Bad Request' });
    expect(spyNavigate).not.toHaveBeenCalled();
  });
});

