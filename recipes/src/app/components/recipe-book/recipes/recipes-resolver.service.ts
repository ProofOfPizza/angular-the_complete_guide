import { isPlatformServer } from '@angular/common';
import { Inject, Injectable, PLATFORM_ID } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { DataStorageService } from 'src/app/shared/services/data-storage.service';
import { Recipe } from './recipe.model';
import { RecipeService } from './recipe.service';

@Injectable({
  providedIn: 'root',
})
export class RecipesResolverService implements Resolve<Array<Recipe>> {
  constructor(
    private dataStorageService: DataStorageService,
    private recipeService: RecipeService,
    @Inject(PLATFORM_ID) private platformId: Object,
  ) {}

  resolve(_route: ActivatedRouteSnapshot, _state: RouterStateSnapshot) {
    if (isPlatformServer(this.platformId)) {
      return [];
    }
    return this.recipeService.recipes.length > 0 ? this.recipeService.recipes : this.dataStorageService.fetchRecipes(); // NB I do not subscribe. The resolver does it for us!
  }
}

