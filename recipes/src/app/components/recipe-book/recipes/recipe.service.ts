import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import { Recipe } from './recipe.model';

@Injectable({
  providedIn: 'root',
})
export class RecipeService {
  public recipesChanged = new Subject<Array<Recipe>>();

  private _recipes: Array<Recipe> = [
    // new Recipe(
    //   'Hot chocolate',
    //   'The best hot chocolate evahhh',
    //   'https://www.natalieshealth.com/wp-content/uploads/2018/11/Gingerbread-Hot-Chocolate-3622.jpg',
    //   [new Ingredient('milk', 250), new Ingredient('cocoa', 50)],
    // ),
    // new Recipe(
    //   'Burgul banadura',
    //   'quick and satisfying!',
    //   'https://madeinmarrow.com/wp-content/uploads/2020/08/B1A5BD7E-5985-4BF0-9361-4E80521378E2_1_201_a-scaled.jpeg',
    //   [new Ingredient('burgul', 300), new Ingredient('tomatoes', 5), new Ingredient('onions', 1)],
    // ),
  ];

  public get recipes() {
    return this._recipes.slice();
  }
  public getRecipeById(index: number) {
    return this.recipes[index];
  }
  public updateRecipe(index: number, recipe: Recipe) {
    this._recipes[index] = recipe;
    this.recipesHaveChanged();
  }

  public addRecipe(recipe: Recipe) {
    this._recipes.push(recipe);
    this.recipesHaveChanged();
  }

  public deleteRecipe(index: number) {
    this._recipes.splice(index, 1);
    const clone = Object.assign([], this.recipes);
    this.recipesChanged.next(clone);
  }

  private recipesHaveChanged() {
    const clone = Object.assign([], this.recipes);
    this.recipesChanged.next(clone);
  }

  public resetRecipesFromServer(recipes: Array<Recipe>) {
    this._recipes = recipes;
    const clone = Object.assign([], this.recipes);
    this.recipesChanged.next(clone);
  }
}

