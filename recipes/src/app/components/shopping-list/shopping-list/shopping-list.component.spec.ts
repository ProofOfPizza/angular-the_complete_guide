import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { fakeAsync, tick } from '@angular/core/testing';
import { Spectator, createComponentFactory } from '@ngneat/spectator';
import { Subject } from 'rxjs';
import { Ingredient } from 'src/app/shared/ingredient.model';
import { ShoppingListComponent } from './shopping-list.component';
import { ShoppingListService } from './shopping-list.service';

describe('ShoppingListComponent', () => {
  let spectator: Spectator<ShoppingListComponent>;
  const createComponent = createComponentFactory({
    component: ShoppingListComponent,
    detectChanges: false,
    mocks: [ShoppingListService],
    schemas: [CUSTOM_ELEMENTS_SCHEMA],
  });

  beforeEach(() => (spectator = createComponent()));

  it('should exist', () => {
    spectator.inject(ShoppingListService).ingredientsChanged = new Subject<Array<Ingredient>>();
    spectator.detectChanges();
    expect(spectator.component).toBeTruthy();
  });

  it('should set the default ingredients from the shoppingListService.', fakeAsync(() => {
    spectator.inject(ShoppingListService).ingredientsChanged = new Subject<Array<Ingredient>>();
    spectator.inject(ShoppingListService)._ingredients = [
      new Ingredient('chocolate', 100),
      new Ingredient('milk', 250),
    ];

    tick();
    spectator.detectChanges();
    expect(spectator.component.ingredients).toContain(new Ingredient('chocolate', 100));
    expect(spectator.component.ingredients).toContain(new Ingredient('milk', 250));
    expect(spectator.component.ingredients.length).toEqual(2);
  }));

  it('should get new changed ingredients from the shoppingListService.', fakeAsync(() => {
    spectator.inject(ShoppingListService).ingredientsChanged = new Subject<Array<Ingredient>>();
    spectator.inject(ShoppingListService)._ingredients = [
      new Ingredient('chocolate', 100),
      new Ingredient('milk', 250),
    ];

    spectator.detectChanges();
    spectator
      .inject(ShoppingListService)
      .ingredientsChanged.next([new Ingredient('chocolate', 200), new Ingredient('warm milk', 250)]);
    tick();
    spectator.detectChanges();
    expect(spectator.component.ingredients).toContain(new Ingredient('chocolate', 200));
    expect(spectator.component.ingredients).toContain(new Ingredient('warm milk', 250));
    expect(spectator.component.ingredients.length).toEqual(2);
  }));

  it('should call the shoppingListService onEdit', () => {
    spectator.inject(ShoppingListService).ingredientsChanged = new Subject<Array<Ingredient>>();
    const shoppingListService = spectator.inject(ShoppingListService);
    shoppingListService.ingredientsChanged = new Subject<Array<Ingredient>>();
    spectator.component.onEditItem(2);
    spectator.detectChanges();
    expect(shoppingListService.selectItem).toHaveBeenCalledWith(2);
  });
});

