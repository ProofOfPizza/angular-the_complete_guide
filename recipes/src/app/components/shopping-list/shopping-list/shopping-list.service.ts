import { Injectable } from '@angular/core';
import { Subject } from 'rxjs/internal/Subject';
import { Ingredient } from 'src/app/shared/ingredient.model';

@Injectable({
  providedIn: 'root',
})
export class ShoppingListService {
  public _ingredients: Array<Ingredient> = [new Ingredient('chocolate', 100), new Ingredient('milk', 250)];
  public ingredientsChanged = new Subject<Array<Ingredient>>();
  public selectedItem = new Subject<number>();

  constructor() {}

  public addIngedientToList(ingredient: Ingredient) {
    this._ingredients.push(ingredient);
    this.ingredientsChanged.next(this.ingredients);
  }

  public updateIngredient(index: number, ingredient: Ingredient) {
    this._ingredients[index] = ingredient;
    this.ingredientsChanged.next(this.ingredients);
  }

  public addIngedientsToList(ingredients: Array<Ingredient>) {
    this._ingredients = [...this._ingredients, ...ingredients];
    this.ingredientsChanged.next(this.ingredients);
  }

  get ingredients() {
    return this._ingredients.slice();
  }

  removeIngredient(index: number) {
    this._ingredients.splice(index, 1);
    this.ingredientsChanged.next(this.ingredients);
  }
  selectItem(index: number) {
    this.selectedItem.next(index);
  }
  getIngredientByIndex(index: number) {
    return this.ingredients[index];
  }
}

