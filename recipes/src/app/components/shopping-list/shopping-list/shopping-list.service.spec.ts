import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { createServiceFactory, SpectatorService } from '@ngneat/spectator';
import { Ingredient } from 'src/app/shared/ingredient.model';
import { ShoppingListService } from './shopping-list.service';

describe('ShoppingListService', () => {
  let spectator: SpectatorService<ShoppingListService>;
  const createService = createServiceFactory({
    service: ShoppingListService,
    schemas: [CUSTOM_ELEMENTS_SCHEMA],
  });

  beforeEach(() => (spectator = createService()));

  it('should exist', () => {
    expect(spectator.service).toBeTruthy();
  });

  it('should #addIngedientToList', () => {
    const ingredient = new Ingredient('beans', 12500);
    spectator.service.addIngedientToList(ingredient);
    expect(spectator.service._ingredients).toContain(ingredient);
  });

  it('should #addIngedientsToList', () => {
    const ingredients = [new Ingredient('beans', 12500), new Ingredient('sugar', 1)];
    spectator.service.addIngedientsToList(ingredients);
    expect(spectator.service._ingredients).toContain(ingredients[0]);
    expect(spectator.service._ingredients).toContain(ingredients[1]);
  });

  it('should #updatedIngredient', () => {
    spectator.service._ingredients = [new Ingredient('flour', 500)];
    const updatedIngredient = new Ingredient('corn flour', 600);
    spectator.service.updateIngredient(0, updatedIngredient);
    expect(spectator.service._ingredients).toContain(updatedIngredient);
  });

  it('should #removeIngredient', () => {
    const [flour, coffee] = [new Ingredient('flour', 500), new Ingredient('coffee', 0.4)];
    spectator.service._ingredients = [flour, coffee];
    spectator.service.removeIngredient(1);
    expect(spectator.service._ingredients).toContain(flour);
    expect(spectator.service._ingredients).not.toContain(coffee);
  });

  it('should #getIngredientByIndex', () => {
    const [flour, coffee, salt] = [
      new Ingredient('flour', 500),
      new Ingredient('coffee', 0.4),
      new Ingredient('salt', 0.02),
    ];
    spectator.service._ingredients = [flour, coffee, salt];
    const item = spectator.service.getIngredientByIndex(1);
    expect(item).toEqual(coffee);
    expect(spectator.service._ingredients.length).toEqual(3);
  });
  it('should #selectItem', () => {
    const spy = spyOn(spectator.service.selectedItem, 'next').and.callThrough();
    spectator.service.selectItem(2);
    expect(spy).toHaveBeenCalledWith(2);
  });
});

