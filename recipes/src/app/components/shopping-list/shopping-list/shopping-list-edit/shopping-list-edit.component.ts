import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Subscription } from 'rxjs';
import { Ingredient } from 'src/app/shared/ingredient.model';
import { ShoppingListService } from '../shopping-list.service';

@Component({
  selector: 'app-shopping-list-edit',
  templateUrl: './shopping-list-edit.component.html',
  styleUrls: ['./shopping-list-edit.component.css'],
})
export class ShoppingListEditComponent implements OnInit, OnDestroy {
  constructor(private shoppingListService: ShoppingListService) {}
  editSubscription: Subscription;
  editmode = false;
  ingredientIndex: number;
  editIngredient: Ingredient;
  @ViewChild('f') shoppingListForm: NgForm;

  ngOnInit(): void {
    this.editSubscription = this.shoppingListService.selectedItem.subscribe((index: number) => {
      this.editIngredient = this.shoppingListService.getIngredientByIndex(index);
      this.ingredientIndex = index;
      this.setForm(this.editIngredient);
      this.editmode = true;
    });
  }

  setForm(ingredient: Ingredient) {
    this.shoppingListForm.setValue({
      name: ingredient.name,
      amount: ingredient.amount,
    });
  }

  ngOnDestroy() {
    this.editSubscription.unsubscribe();
  }

  onSubmit(form: NgForm) {
    const ingredient = new Ingredient(form.value.name, form.value.amount);
    if (!this.editmode) {
      this.shoppingListService.addIngedientToList(ingredient);
    } else {
      this.shoppingListService.updateIngredient(this.ingredientIndex, ingredient);
    }
    this.onClear();
  }
  onDelete() {
    this.shoppingListService.removeIngredient(this.ingredientIndex);
    this.onClear();
  }
  onClear() {
    this.shoppingListForm.reset();
    this.editmode = false;
  }
}

