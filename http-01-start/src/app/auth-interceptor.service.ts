import { HttpEventType, HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { tap } from 'rxjs/operators';

export class AuthInterceptorService implements HttpInterceptor {
  intercept(request: HttpRequest<any>, next: HttpHandler) {
    if (request.url === 'somespecialurl') {
      console.log('intercepting request at  special url');
    } else {
      const modifiedRequest = request.clone({ headers: request.headers.append('Shiny-Header-Auth', 'xyz') });
      console.log('intercept request at normal url');
      return next.handle(modifiedRequest).pipe(
        tap((event) => {
          if (event.type === HttpEventType.Response) {
            console.log('intercepting response now, look: ', event.body);
          }
        }),
      );
    }
    return next.handle(request);
  }
}

