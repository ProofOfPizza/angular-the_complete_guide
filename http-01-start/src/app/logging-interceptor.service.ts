import { HttpEventType, HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { tap } from 'rxjs/operators';
import { match } from 'ts-pattern';

export class LogginInterceptorService implements HttpInterceptor {
  intercept(req: HttpRequest<any>, next: HttpHandler) {
    if (req.method === 'POST') {
      console.log('logging the post request:', req.body, req.url);
    }
    return next.handle(req).pipe(
      tap((event) => {
        console.log(event);
        match(event)
          .with({ type: HttpEventType.Response }, () => {
            if (event.type === HttpEventType.Response) {
              console.log('success', event.body);
            }
          })
          .otherwise(() => console.log('...otherwise', event));
      }),
    );
  }
}

