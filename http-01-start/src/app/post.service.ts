import { Injectable } from '@angular/core';
import { HttpClient, HttpEventType, HttpHeaders, HttpParams } from '@angular/common/http';
import { catchError, map, tap } from 'rxjs/operators';
import { Post } from './post.modal';
import { Observable, Subject, throwError } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class PostService {
  postsUrl = 'https://angular-complete-course-70e8d-default-rtdb.europe-west1.firebasedatabase.app/posts.json';
  error = new Subject<Error>();

  constructor(private http: HttpClient) {}

  createPost(post: Post) {
    this.http
      .post<Post>(this.postsUrl, post, {
        observe: 'events', //'body', // 'response',
        responseType: 'json', // 'text' , 'blob'//etc --> json is inferred here from post generic type
      })
      .pipe(
        tap((event) => {
          if (event.type === HttpEventType.Sent) {
            console.log(event);
          }
        }),
      )
      .subscribe(
        (data) => {
          console.log(data);
          return data;
        },
        (err) => {
          err.statusText = err.statusText + ' posting went wrong';
          this.error.next(err);
        },
      );
  }

  fetchPosts(): Observable<any> {
    return this.http
      .get<{ [key: string]: Post }>(this.postsUrl, {
        headers: new HttpHeaders({
          CustomXHeader: 'Awesome',
        }),
        params: new HttpParams().set('print', 'pretty'),
      })
      .pipe(
        map((postsObject) => {
          return Object.keys(postsObject).map((key) => {
            return { ...postsObject[key], id: key };
          });
        }),
        catchError((errorRes) => {
          //send to analytics bla bla
          errorRes.bla = 'bla';
          return throwError(errorRes);
        }),
      );
  }

  clearPosts() {
    return this.http.delete(this.postsUrl);
  }
}

