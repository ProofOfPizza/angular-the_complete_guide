import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { Post } from './post.modal';
import { PostService } from './post.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent implements OnInit, OnDestroy {
  loadedPosts: Array<Post> = [];
  isLoading = false;
  error = null;
  errorSubscription: Subscription;
  constructor(private postService: PostService) {}

  ngOnInit() {
    this.isLoading = true;
    this.postService.fetchPosts().subscribe(
      (data: Array<Post>) => {
        this.loadedPosts = data;
        this.isLoading = false;
      },
      (error) => {
        this.isLoading = false;
        this.error = error;
        console.error(error);
      },
    );
    this.errorSubscription = this.postService.error.subscribe((err) => {
      this.error = err;
    });
  }

  onCreatePost(postData: Post) {
    this.postService.createPost(postData);
  }

  onFetchPosts() {
    this.isLoading = true;
    this.postService.fetchPosts().subscribe(
      (data: Array<Post>) => {
        this.loadedPosts = data;
        this.isLoading = false;
      },
      (error) => {
        this.isLoading = false;
        this.error = error;
        console.error(error);
      },
    );
  }

  async onClearPosts() {
    this.isLoading = true;
    this.postService.clearPosts().subscribe((result) => {
      console.log(result);
      this.postService.fetchPosts().subscribe(
        (data: Array<Post>) => {
          this.loadedPosts = data;
          this.isLoading = false;
        },

        (error) => {
          console.log(error);
          this.error = error;
        },
      );
    });
  }

  onHandleError() {
    this.error = null;
  }
  ngOnDestroy() {
    this.errorSubscription.unsubscribe();
  }
}

