import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'filter',
  pure: false,
})
export class FilterPipe implements PipeTransform {
  transform(value: string | Array<any>, filterString: string, prop: string): unknown {
    console.log('impure');
    if (value.length === 0 || filterString.length === 0) {
      return value;
    }
    return [...value].filter((item: any) => item[prop] === filterString);
  }
}

