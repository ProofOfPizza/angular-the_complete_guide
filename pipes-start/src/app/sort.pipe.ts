import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'sort',
})
export class SortPipe implements PipeTransform {
  transform(value: Array<{}>, prop: string): unknown {
    return value.sort((a, b) => {
      return a[prop] > b[prop] ? 1 : -1;
    });
  }
}

