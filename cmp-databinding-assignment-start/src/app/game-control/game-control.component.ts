import { Component, EventEmitter, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-game-control',
  templateUrl: './game-control.component.html',
  styleUrls: ['./game-control.component.css'],
})
export class GameControlComponent implements OnInit {
  count: number = 0;
  running: boolean = false;
  @Output() gameEvent = new EventEmitter<number>();

  constructor() {}

  ngOnInit(): void {}

  onStart() {
    this.running = true;
    this.emitEvents();
  }
  onPause() {
    this.running = false;
  }
  emitEvent() {
    this.count++;
    this.gameEvent.emit(this.count);
  }
  emitEvents() {
    setInterval(() => {
      if (!this.running) {
        return;
      }
      this.emitEvent();
    }, 1000);
  }
}

