import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { GameControlComponent } from './game-control/game-control.component';
import { OddComponent } from './number/odd/odd.component';
import { EvenComponent } from './number/even/even.component';
import { NumberComponent } from './number/number.component';

@NgModule({
  declarations: [AppComponent, GameControlComponent, OddComponent, EvenComponent, NumberComponent],
  imports: [BrowserModule, FormsModule],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}

