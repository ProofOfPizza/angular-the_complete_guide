import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-number',
  templateUrl: './number.component.html',
  styleUrls: ['./number.component.css'],
})
export class NumberComponent implements OnInit {
  @Input('num') num: number;

  ngOnInit(): void {}

  isOdd(): boolean {
    return this.num % 2 === 1;
  }
}

