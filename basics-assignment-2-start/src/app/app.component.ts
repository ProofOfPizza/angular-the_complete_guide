import { Component } from '@angular/core';
import { FormsModule } from '@angular/forms';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent {
  userName: string;
  lastUserName: string;

  onReset() {
    this.lastUserName = this.userName;
    this.userName = '';
  }
  displayUsername() {
    return this.lastUserName;
  }
}

