import { Directive, ElementRef, HostBinding, HostListener, Input, OnInit, Renderer2 } from '@angular/core';

@Directive({
  selector: '[appBetterHighlight]',
})
export class BetterHighlightDirective implements OnInit {
  @HostBinding('style.color') color: string; //another decorator, to make life easier.
  @Input() defaultBackroundColor: string = 'transparent';
  @Input() highlightBackgroundColor: string = 'blue';
  constructor(private renderer: Renderer2, private elementRef: ElementRef) {}

  ngOnInit() {
    this.renderer.setStyle(this.elementRef.nativeElement, 'background-color', this.defaultBackroundColor);
  }
  @HostListener('mouseenter') mouseover(eventData: Event) {
    this.renderer.setStyle(this.elementRef.nativeElement, 'background-color', this.highlightBackgroundColor);
    this.color = 'yellow';
  }
  @HostListener('mouseleave') mouseleave(eventData: Event) {
    //argument niet nodif, maar hier zodat we weten dat we die kunnen krijgen
    this.renderer.setStyle(this.elementRef.nativeElement, 'background-color', this.defaultBackroundColor);
    this.color = 'black';
  }
}

